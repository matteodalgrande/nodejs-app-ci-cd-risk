///prova
'use strict';
//Lo scopo "use strict"è quello di indicare che il codice deve essere eseguito in "modalità rigorosa".
//Con la modalità rigorosa, ad esempio, non è possibile utilizzare variabili non dichiarate.

const express = require('express');

// Constants
const PORT = process.env.PORT || 3000;
const HOST = '0.0.0.0';

// App
const app = express();
app.get('/', (req, res) => {
  res.send('Hello World');
});

app.listen(PORT, HOST, ()=>{
	console.log(`Running on http://${HOST}:${PORT}`);
});
