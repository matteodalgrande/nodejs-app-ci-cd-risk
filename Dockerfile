# specify the node base image with your desired version node:<version>-->this is the version of docker
FROM node:10

#WORKDIR /app
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

#Install the dipendences
COPY package.json /usr/src/app
RUN npm install
COPY . /usr/src/app
CMD node server.json

# replace this with your application's default port
EXPOSE 3000
CMD ["npm", "start"]
